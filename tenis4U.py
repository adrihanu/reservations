import sys, logging
import urllib.parse
import time, json, datetime, requests
from requests.auth import HTTPBasicAuth

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s.%(msecs)03d %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
#logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

username = "adrihanu@gmail.com"
password = ""

date = '2019/08/31'
hour = 20

#PAYMENT
first_name = "Adrian"
last_name = "Hanusiak"
email = "adrihanu@gmail.com"

card_number = "4574535632153633"
card_month = "08"
card_year = "2037"
card_cvc = "231"

useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/76.0.3809.100 Chrome/76.0.3809.100 Safari/537.36"

#Start session to keep cookies
s = requests.Session() 

def get_access_token():
    """ Login into the website to get access token."""
    url = "https://api.tenis4u.pl/oauth/token";
    headers = {'User-Agent': useragent}
    data = {'username': username, "password": password, "grant_type": "password"}  
    response = s.post(url, data=data, headers=headers, auth=HTTPBasicAuth('tenis', 'secret'))
    try:
        json_data = json.loads(response.text)
        access_token = json_data['access_token']
        logging.info('Logged in.')
        return access_token
    except:
        logging.info('Failed to login.')
        exit()


def check_token(token):
    logging.info('Checking token.')
    url = "https://api.tenis4u.pl/whoami"
    headers = {'Authorization': 'Bearer ' + token,
               'User-Agent': useragent}
    response = s.get(url, headers=headers) 
    json_data = json.loads(response.text)
    if username == json_data['email']:
        logging.info('Correct token.')
        return True
    else: 
        logging.info('Incorrect token.')
        return False
   
def check_court():
    url = "https://api.tenis4u.pl/court/3"
    headers = {'User-Agent': useragent}
    response = s.get(url, headers=headers) 
    json_data = json.loads(response.text)
    return  json_data['name'] == 'Kort w Parku Jordana'

def get_schedule():
    logging.info('Loading schedule.')
    url = "https://api.tenis4u.pl/occupancy/3"
    response = s.get(url) 
    json_data = json.loads(response.text)
    days = json_data['stations'][0]['days']
    logging.info(days)
    return days

def is_date_available(schedule, date):
    for day in schedule:
        if day['date'] == date:
            logging.info('Day is avilable')
            return True

    logging.info('Day is not avilable')
    return False

def is_hour_available(schedule, date, hour):
    for day in schedule:
        if day['date'] == date:
            free_hours = day['free_hours']
            for free_hour in free_hours:
                if hour >= free_hour['begin']['hour'] and hour < free_hour['end']['hour']:
                    logging.info('Hour is avilable')
                    return True
    logging.info('Hour is not avilable.')
    return False


def make_reservation(token):
    logging.info('Making reservation.')
    url = "https://api.tenis4u.pl/reservation"
    headers = {'Authorization': 'Bearer ' + token,
               'User-Agent': useragent,
               'Content-Type': 'application/json'}
    data = {"starts_at":date.replace('/','-')+"T"+str(hour)+":00:00.000Z","ends_at":date.replace('/','-')+"T"+str(hour+1)+":00:00.000Z","station_id":24,"payment_method":"ONLINE","tariff_id":12,"multisport_cards":0}
    data=json.dumps(data)
    logging.info(data)

    response = s.post(url, data=data, headers=headers) 
    json_data = json.loads(response.text)
    logging.info(json_data)
    return json_data

def payment(reservation):
    logging.info("Payment.")
    reservation = reservation['payment']
    data= {  
      "id": reservation['shop_id'],
      "kwota": 2,
      "opis": reservation['description'],
      "control": reservation['control'],
      "url": "https://app.tenis4u.pl/#/court/map/start"
    }
    logging.info(data)

    params =  urllib.parse.urlencode(data, quote_via=urllib.parse.quote)
    environment = "https://ssl.dotpay.pl/t2/"
    url = environment+"/?"+params

    logging.info(url)

    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    driver = webdriver.Chrome('./chromedriver', chrome_options=options)
    driver.get(url);
    payment_cards  = driver.find_element_by_id('channel_image_248')
    payment_cards.click()

    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, 'id_dp-firstname')))

    first_name_element = driver.find_element_by_id('id_dp-firstname')
    first_name_element.send_keys(first_name)

    last_name_element = driver.find_element_by_id('id_dp-lastname')
    last_name_element.send_keys(last_name)

    email_element = driver.find_element_by_id('id_dp-email')
    email_element.send_keys(email)

    card_number_element = driver.find_element_by_id('id_dp-number')
    for digit in card_number:
        card_number_element.send_keys(digit)

    card_month_element = driver.find_element_by_id('id_dp-expiry_month')
    card_month_element.send_keys(card_month)

    card_year_element = driver.find_element_by_id('id_dp-expiry_year')
    card_year_element.send_keys(card_year)

    card_cvc_element = driver.find_element_by_id('id_dp-cvc')
    card_cvc_element.send_keys(card_cvc)

    driver.save_screenshot('tmp/screenshot_1_'+str(datetime.datetime.now())+'.png');

    payment_button = driver.find_element_by_id('payment-form-submit-dp')
    payment_button.click()
    driver.save_screenshot('tmp/screenshot_2_'+str(datetime.datetime.now())+'.png');
    WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.ID, 'operation-status')))
    driver.save_screenshot('tmp/screenshot_3_'+str(datetime.datetime.now())+'.png');
    WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.CSS_SELECTOR, '.transaction-ok')))
    driver.save_screenshot('tmp/screenshot_4_'+str(datetime.datetime.now())+'.png');
    driver.quit()

if not password: exit('Password not provided')

access_token = None
while True:
    if datetime.datetime.utcnow().hour == 22:#utc
        logging.info('Detected midnight gmt+2.')
        schedule = get_schedule()
        if is_date_available(schedule, date):
            if is_hour_available(schedule, date, hour):
                if not access_token or not check_token(access_token): access_token = get_access_token()   
                reservation = make_reservation(access_token)
                payment(reservation)
                exit('Done')
            else: exit('Hour taken.')
        else: 
            logging.info('Waiting for schedule')
            time.sleep(1)#Not yet available
    else: 
        logging.info('Waiting for midnight gmt+2.')
        time.sleep(1)
