import smtplib, ssl
import calendar
import json, datetime, requests, time
from bs4 import BeautifulSoup

start_hour = datetime.datetime.strptime("19:00", '%H:%M').time()
end_hour = datetime.datetime.strptime("22:00", '%H:%M').time()
#Start session to keep cookies
s = requests.Session() 

username = "adrihanu@gmail.com"
password = "Adrian"#input("Type your TwojTenis password and press enter: ")

#EMAIL
port = 465  # For SSL
smtp_server = "smtp.gmail.com"
sender_email = "adrihanu@gmail.com"  # Enter your address
receiver_email = "adrihanu@gmail.com"  # Enter receiver address
password_email = "Adrian"#input("Type your email password and press enter: ")

def send_email(dates):
    print("Sending email", flush=True)
    message = """\
Subject: TwojTenis notification

Tenis available:\n  ---"""+'\n  ---'.join([d.strftime("%A %d/%m, %H:%M") for d in dates['tenis']])+"""
Badminton available:\n  ---"""+'\n  ---'.join([d.strftime("%A %d/%m, %H:%M") for d in dates['badminton']])
        

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password_email)
        server.sendmail(sender_email, receiver_email, message)
        print("Email sent.", flush=True)

def login():
    """ Login into the website."""
    url = "https://www.twojtenis.pl/pl/login.html";
    data = {"login": username, "pass": password, "action": "login"}  
    s.post(url, data=data)

def get_schedule(date):
    """ Download schedule form the website"""
    data = {"date": date, "club_url": "fame_sport_club", "page": "NaN", "spr": 3, "zsh": 0, "tz": 0}
    url = "https://www.twojtenis.pl/ajax.php?load=courts_list"
    response = s.post(url, data=data)

    #Parse schedule
    try:
        json_data = json.loads(response.text)
        schedule = json_data['schedule']
        return schedule
    except:
        login()
        return get_schedule(date)


#url="/home/adrian/t.html"
#page = open(url)
#soup = BeautifulSoup(page.read(), 'html.parser')

available_dates = {"tenis":[], "badminton":[]}

while True:
    notify = False
    now = datetime.datetime.now()
    start_day = datetime.datetime.today().day+1
    end_day = calendar.monthrange(now.year, now.month)[1]
    if start_day > end_day: continue
    for day in range(start_day, end_day)[:7]:
        try:
            date = datetime.datetime.strptime(str(day)+'.'+str(now.month)+'.'+str(now.year), '%d.%m.%Y')
        
            dayweek = date.strftime("%A")
            if dayweek in ['Friday', 'Saturday', "Sunday"]: continue
            date = date.strftime("%d.%m.%Y")
            print("Date", date, flush=True)
            soup = BeautifulSoup(get_schedule(date), 'html.parser')

            #Find main columns with courts
            courts = soup.findAll("div", {"class": "schedule_col"})

            for court in courts:
                reserved = 0; # Variable used to skip reserved houres
                try:
                    rows = court.findAll("div") # Rows with houres and reservations
                    court_title = rows[0].find('strong').text # First row is a title
                except:
                    continue
                
                #Detect sport
                if 'Kort' in court_title: sport = "tenis"
                elif 'Badminton' in court_title: sport = "badminton"         
                else: continue

                court_number = court_title.split()[-1] # Extract court number
                print("Check court", court_number, "for", sport, flush=True)
                for row in rows[1:]:
                    classes = row.attrs['class'] # Extract classes of row
                    if "reservation_closed" in classes: # Row is a reservation
                        style = row.get('style').split(';') # Extract style
                        for attr in style: # Extract attributes from style
                            if 'height' in attr: # For height attribute
                                val = attr.strip('height:').strip('px') # Extract value
                                reserved = int(val) // 41 # Split by 41px, to get quantity of units reserved

                    elif "schedule_row" in classes: # Row is a time unit(houres)
                        hour = row.find('span').find('strong').text.split()[1] # Extract hour
                        if reserved: # If this unit is reserved
                            reserved -= 1 # Decrement reserved units by one
                            continue # Skip this unit
                        else:
                            #print(hour)
                            available_hour = datetime.datetime.strptime(hour, '%H:%M').time()
                            if available_hour >= start_hour and available_hour < end_hour:
                                available_date = datetime.datetime.strptime(date+' '+hour, '%d.%m.%Y %H:%M')
                                if available_date not in available_dates[sport]:
                                    available_dates[sport].append(available_date)
                                    notify = True
                                    print("New date:", available_date, flush=True)
        except Exception as e: 
            print(e)
            continue
    if notify:
        send_email(available_dates)
    time.sleep(60*5)            
