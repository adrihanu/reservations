FROM python:3.6

WORKDIR /reservations
RUN apt-get update 
RUN apt-get install -y chromium

COPY . .

RUN pip install pipenv
RUN pipenv install
