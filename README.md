# Reservations

Scripts for automating reservation process.

It helps making a reservation for requested time, whenever someone cancel their booking.

Tags: `Python`, `Selenium`, `Docker`, `Docker Compose`

## Running

```bash
docker-compose up -d
```